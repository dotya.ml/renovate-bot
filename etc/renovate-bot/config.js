module.exports = {
  endpoint: 'https://git.dotya.ml/api/v1/',
  platform: 'gitea',
  username: 'renovate-bot',
  name: 'renovate-bot',

  // Gitea access token of Renovate Bot user
  token: process.env.RENOVATE_TOKEN,

  // gpg signing key for Renovate-Bot-authored commits
  gitPrivateKey: process.env.GPG_KEY,

  // process repos with renovate config (and befriended with Renovate Bot)
  autodiscover: true,

  requireConfig: "required",
  onboarding: true,
  onboardingConfig: {
    extends: ['config:base', ':dependencyDashboard'],
  },
  onboardingBranch: 'renovate/configure',

  prFooter: process.env.RENOVATE_PR_FOOTER,

  binarySource: 'docker',
  dockerImagePrefix: 'ghcr.io/renovatebot',
  dockerUser: process.env.RENOVATE_DOCKER_USER,

  gitAuthor: process.env.RENOVATE_GIT_AUTHOR,
  git_author: process.env.RENOVATE_GIT_AUTHOR,

  // explicitly list enabled managers
  "enabledManagers": [
    "ansible",
    "docker-compose",
    "dockerfile",
    "droneci",
    "kubernetes",
    "gomod",
    "pip_requirements",
    "pip_setup",
    "poetry",
    "cargo",

    // the following ones are still in beta (as of 2022-07-04)
    "ansible-galaxy",
    "buildkite",
    "git-submodules",
    "helm-requirements",
    "helm-values",
    "helmfile",
    "pre-commit",
    "terraform",
    "terraform-version",
  ],

  logLevel: 'verbose',
  optimizeForDisabled: true
};

# [`renovate-bot`](https://git.dotya.ml/dotya.ml/renovate-bot)

this repo holds configuration files for `dotya.ml`-operated
[`renovate`](https://github.com/renovatebot/renovate)
[bot](https://git.dotya.ml/renovate-bot).

### usage
fill (or otherwise provide to the process) desired environment variables and
run `bin/renovatepls` regularly (ideally with `cron` or using a `systemd`
timer).

`renovatepls` is just a simple bash script that does just that: sets some env
vars (might need to modify them, e.g. for using with your own instance of
Gitea...) and then executes `renovate`.
